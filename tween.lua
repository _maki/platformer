require 'class'

tween = class:child()
local tweens = setmetatable({}, {__mode = 'k'})

function tween:init(get, set, res, time)
	tweens[self] = true
	local start = get()
	self.get = get
	self.set = set
	self.res = res
	self.diff = (res - start) / time
end

function tween:update(dt)
	if (self.diff > 0 and self.get() < self.res) or (self.diff < 0 and self.get() > self.res) then
		self.set(self.get() + self.dif * dt)
	end

	if (self.diff > 0 and self.get() >= self.res) or (self.diff < 0 and self.get() <= self.res) then
		self.set(self.res)
		tweens[self] = nil
	end
end

function updateTweens(dt)
	for twn in pairs(tweens) do
		twn:update(dt)
	end
end
