require 'class'

button = class:child()

function button:init(x, y, w, h, func, stateList)
	self.x = x
	self.y = y
	self.w = w
	self.h = h
	self.func = func
	self.isActive = false
	self.stateList = stateList
end

function button:isMouseOver(mx, my)
	mx = mx or love.mouse.getX()
	my = my or love.mouse.getY()
	return mx > self.x and mx < self.x + self.w and my > self.y and my < self.y + self.h
end

function button:isMouseDown(mx, my)
	return button:isOver(mx, my) and love.mouse.isDown 'l'
end

function button:checkMouse() -- for mousepressed or mousereleased
	if self:isMouseOver() then
		self.func()
	end
end

function button:update()
	if self:isMouseOver() then
		for _, b in pairs(self.stateList) do
			b.isActive = false
		end

		self.isActive = true
	end
end
