require 'button'

mainmenu = {}

function mainmenu:load(moderef)
	self.globalmoderef = moderef

	self.main = {}
	self.main.play = button(love.graphics.getWidth()/2 - 32, love.graphics.getHeight()/2, 64, 64, function() end, self.main)
	self.main.quit = button(love.graphics.getWidth()*3/4 - 32, love.graphics.getHeight()/2, 64, 64, love.event.quit, self.main)
	self.main.options = button(love.graphics.getWidth()/4 - 32, love.graphics.getHeight()/2, 64, 64, function() self.mode = self.options end, self.main)

	self.options = {}
	self.options.fullscreen = button(love.graphics.getWidth()/3 - 32, love.graphics.getHeight()/2, 64, 64, function() love.window.setFullscreen(not love.window.getFullscreen()) end, self.options)
	self.options.quit = button(love.graphics.getWidth()*2/3 - 32, love.graphics.getHeight()/2, 64, 64, function() self.mode = self.main end, self.options)

	self.mode = self.main

	self.images = {}
	self.images.play = love.graphics.newImage 'gfx/play.png'
	self.images.quit = love.graphics.newImage 'gfx/quit.png'
	self.images.options = love.graphics.newImage 'gfx/gear.png'
	self.images.fullscreen = love.graphics.newImage 'gfx/fullscreen.png'
	self.images.windowed = love.graphics.newImage 'gfx/windowed.png'
end

function mainmenu:update(dt)
	if self.mode == self.main then
		for _, b in pairs(self.main) do
			b:update()
		end
	elseif self.mode == self.options then
		for _, b in pairs(self.options) do
			b:update()
		end
	end
end

function mainmenu:mousereleased(x, y, b)
	if self.mode == self.main then
		for _, b in pairs(self.main) do
			b:checkMouse()
		end
	elseif self.mode == self.options then
		for _, b in pairs(self.options) do
			b:checkMouse()
		end
	end
end

function mainmenu:draw()
	local function drawButton(button, image)
		if button.isActive then
			love.graphics.setColor(255, 255, 255)
		else
			love.graphics.setColor(100, 100, 100)
		end

		love.graphics.rectangle('fill', button.x, button.y, button.w, button.h)
		love.graphics.draw(image, button.x, button.y)
	end

	if self.mode == self.main then
		drawButton(self.main.play, self.images.play)
		drawButton(self.main.quit, self.images.quit)
		drawButton(self.main.options, self.images.options)
	elseif self.mode == self.options then
		if love.window.getFullscreen() then
			drawButton(self.options.fullscreen, self.images.windowed)
		else
			drawButton(self.options.fullscreen, self.images.fullscreen)
		end
		drawButton(self.options.quit, self.images.quit)
	end
end
