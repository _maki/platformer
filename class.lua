class = {}
class.__index = class

function class:child()
	local o = setmetatable({}, self)
	o.__index = o
	return o
end

function class:__call(...)
	local o = setmetatable({}, self)
	if o.init then o:init(...) end
	return o
end
