require 'class'
require 'tween'
require 'mainmenu'

function love.load()
	love.physics.setMeter(64)
	mode = {mainmenu}
	mainmenu:load(mode)
end

function love.update(dt)
	updateTweens(dt)
	if mode[1].update then
		mode[1]:update(dt)
	end
end

function setCallback(name)
	love[name] = function(...)
		if mode[1][name] then
			mode[1][name](mode[1], ...)
		end
	end
end

setCallback 'draw'
setCallback 'keypressed'
setCallback 'keyreleased'
setCallback 'mousepressed'
setCallback 'mousereleased'
setCallback 'focus'
setCallback 'resize'
setCallback 'visable'
setCallback 'mousefocus'
setCallback 'textinput'
