io.stdout:setvbuf 'no'

function love.conf(t)
	t.window.fullscreentype = 'desktop'
	t.window.srgb = false
	t.window.title = 'Platformer'
	t.window.fsaa = 2
	t.window.icon = "gfx/icon.png"
end
